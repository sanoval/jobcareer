<?php   

class Jobs extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('job');
    }

    public function index() {
        $this->template->load('templates/backend', 'contents/backend/jobs', null);
    }

    public function api() {
        $data = $this->job->show_all_api();
        header('Content-Type: text/json');
        header("Access-Control-Allow-Origin: *");
        echo '('.json_encode($data).')';
    }

	public function get_data() {
		header('Content-Type: application/json');
		$result = $this->job->show_all();
		echo $result;
    }
    
    public function submit() {
        $data = array(
            'title'       => $this->input->post('job_title'),
            'code'        => $this->input->post('job_code'),
            'jobdesc'     => nl2br($this->input->post('job_desc')),
            'requirement' => nl2br($this->input->post('job_req'))
        );
        $res = $this->job->add_new($data);

        if ($res === 1) {
            echo json_encode(['status' => 'ok', 'message' => 'insert successful']);
        } else {
            echo json_encode(['status' => 'error', 'message' => 'insert failed']);
        }
    }

    public function detail($id) {

        $data = array();

        $data = $this->job->show($id);

        echo json_encode(array('status' => 'ok', 'data' => $data));
    }
}