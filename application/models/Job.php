<?php

class Job extends CI_Model {

    const JOB = 'jobs';

    function __construct() {
        parent::__construct();
    }

    public function show_all() {
        $this->datatables->select('*')
            ->from(Job::JOB);
        $this->db->order_by('id', 'desc');
        
        return $this->datatables->generate();
    }

    public function show_all_api() {
        return $this->db->get(Job::JOB)->result();
    }

    public function add_new($serialize) {
        $this->db->insert(Job::JOB, $serialize);
        return $this->db->affected_rows();
    }

    public function update($id, $serialize) {
        return $this->db->where('id', $id)
            ->update(Job::JOB, $serialize);
    }

    public function delete($id) {
        return $this->db->where('id', $id)
            ->delete(Job::JOB);
    }

    public function show($id) {
        return $this->db->get_where(Job::JOB, array('id' => $id))
            ->row();
    }
}