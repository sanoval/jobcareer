<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Career Dashboard</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Main CSS-->
    <link rel="stylesheet" type="text/css" href="<?php echo '/assets/css/main.css'; ?>">
    <!-- Font-icon css-->
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  </head>
  <body class="app sidebar-mini rtl">
    <!-- Navbar-->
    <header class="app-header"><a class="app-header__logo" href="index.html">Career</a>
      <!-- Sidebar toggle button--><a class="app-sidebar__toggle" href="#" data-toggle="sidebar" aria-label="Hide Sidebar"></a>
      <!-- Navbar Right Menu-->
      <ul class="app-nav">
        <!-- User Menu-->
        <li class="dropdown"><a class="app-nav__item" href="#" data-toggle="dropdown" aria-label="Open Profile Menu"><i class="fa fa-user fa-lg"></i></a>
          <ul class="dropdown-menu settings-menu dropdown-menu-right">
            <li><a class="dropdown-item" href="page-login.html"><i class="fa fa-sign-out fa-lg"></i> Logout</a></li>
          </ul>
        </li>
      </ul>
    </header>

    <!-- Sidebar menu-->
    <div class="app-sidebar__overlay" data-toggle="sidebar"></div>
    <aside class="app-sidebar">
      <div class="app-sidebar__user"><img class="app-sidebar__user-avatar" src="https://s3.amazonaws.com/uifaces/faces/twitter/jsa/48.jpg" alt="User Image">
        <div>
          <p class="app-sidebar__user-name">Sanoval Awwalin</p>
          <p class="app-sidebar__user-designation">Frontend Developer</p>
        </div>
      </div>
      <ul class="app-menu">
        <li><a class="app-menu__item <?php echo $this->uri->segment(1)==''?'active':''; ?>" href="#"><i class="app-menu__icon fa fa-newspaper-o"></i><span class="app-menu__label">Jobs</span></a></li>
      </ul>
    </aside>
    <!-- /Sidebar menu -->

    <!-- Main -->
    <?php echo $contents; ?>
    <!-- /Main -->
    <!-- Essential javascripts for application to work-->
    <script src="<?php echo '/assets/js/jquery-3.2.1.min.js'; ?>"></script>
    <script src="<?php echo '/assets/js/popper.min.js'; ?>"></script>
    <script src="<?php echo '/assets/js/bootstrap.min.js'; ?>"></script>
    <script src="<?php echo '/assets/js/main.js'; ?>"></script>
    <!-- The javascript plugin to display page loading on top-->
    <script src="<?php echo '/assets/js/plugins/pace.min.js'; ?>"></script>
    <!-- Datatables -->
    <script src="<?php echo '/assets/js/plugins/jquery.dataTables.min.js'; ?>"></script>
    <script src="<?php echo '/assets/js/plugins/dataTables.bootstrap.min.js'; ?>"></script>
  </body>
</html>