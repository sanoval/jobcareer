<main class="app-content">
    <div class="app-title">
        <div>
            <h1><i class="fa fa-newspaper-o"></i> Jobs</h1>
            <p>List of jobs</p>
        </div>
        <ul class="app-breadcrumb breadcrumb">
            <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
            <li class="breadcrumb-item"><a href="#">Jobs</a></li>
        </ul>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="tile">
                <div class="tile-title-w-btn">
                    <h3 class="title">All Jobs</h3>
                    <p><a class="btn btn-primary icon-btn" onclick="clearNew()" href="#modal-form" data-toggle="modal"><i class="fa fa-plus"></i>Add Job</a></p>
                </div>
                <div class="tile-body">
                    <table id="main-table" class="table table-hover table-bordered" style="width:100%">
                        <thead>
                            <tr>
                                <th>Title</th>
                                <th>Code</th>
                                <th>Jobdesc</th>
                                <th>Requrements</th>
                                <th>Status</th>
                                <th></th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</main>

<!-- The Modal -->
<div class="modal fade" id="modal-form">
    <div class="modal-dialog">
        <div class="modal-content">
            <form id="form-job">
                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title" id="modal-title">Add Job</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <!-- Modal body -->
                <div class="modal-body">
                    <div class="form-group">
                        <label for="job-title">Job Title:</label>
                        <input type="hidden" name="job_id" id="job-id">
                        <input type="text" class="form-control" id="job-title" name="job_title" required>
                    </div>
                    <div class="form-group">
                        <label for="job-code">Code:</label>
                        <input type="text" class="form-control" id="job-code" name="job_code" required>
                    </div>
                    <div class="form-group">
                        <label for="job-desc">Job Description:</label>
                        <textarea class="form-control" id="job-desc" name="job_desc" rows="6" required></textarea>
                    </div>
                    <div class="form-group">
                        <label for="job-req">Requirements:</label>
                        <textarea class="form-control" id="job-req" name="job_req" rows="6" required></textarea>
                    </div>
                </div>
                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script>
    document.addEventListener('DOMContentLoaded', function() {
        console.log('loaded');
        var table = $('#main-table').DataTable({
            "processing": true,
            "serverSide": true,
            "ajax": "<?php echo base_url('jobs/get_data'); ?>",
            "autoWidth": false,
            "columns": [{
                    "data": "title"
                },
                {
                    "data": "code"
                },
                {
                    "data": "jobdesc"
                },
                {
                    "data": "requirement"
                },
                {
                    "data": null,
                    "render": function(data, type, row) {
                        return data.status == 1 ? 'Active' : 'Inactive';
                    }
                },
                {
                    "data": null,
                    "width": "10%",
                    "searchable": false,
                    "orderable": false,
                    "render": function(data, type, row) {
                        return "<div class=\"btn-group\" role=\"group\">" +
                            "<button type=\"button\" class=\"btn btn-primary\">Edit</button>" +
                            "<button type=\"button\" class=\"btn btn-primary\">Delete</button>" +
                            (data.status == 1 ? "<button onclick=\"goDeactivate(" + data.id + ")\" type=\"button\" class=\"btn btn-primary\">Deactivate</button>" : "<button onclick=\"goActivate(" + data.id + ")\" type=\"button\" class=\"btn btn-primary\">Activate</button>") +
                            "</div>";
                    }
                }
            ]
        });

        // form submit
        $('#form-job').submit(function(e) {
            e.preventDefault();
            $.ajax({
                type: 'POST',
                url: '<?php echo base_url('jobs/submit') ?>',
                contentType: false,
                dataType: false,
                processData: false,
                data: new FormData(this),
                success: function(response) {
                    $('#modal-form').modal('hide');
                    table.ajax.reload(null, false);
                }
            })
        });
    })

    function goEdit(id) {
        $.ajax({
            type: 'GET',
            url: '<?php echo base_url('jobs/detail'); ?>/' + id,
            contentType: false,
            dataType: false,
            processData: false,
            dataType: 'json',
            success: function(response) {
                var data = response.data;
                var regex = /<br\s*[\/]?>/gi;
                $('#modal-form').modal('show');
                $('#job-id').val(data.id);
                $('#job-title').val(data.title);
                $('#job-code').val(data.code);
                $('#job-desc').val(data.jobdesc.replace(regex, ""));
                $('#job-req').val(data.requirement.replace(regex, ""));
                table.ajax.reload(null, false);
            }
        })
    }

    function clearNew() {
        document.getElementById("form-job").reset();
        $('#modal-title').html('Add Job');
        $('#job-id').val('');
    }
</script> 