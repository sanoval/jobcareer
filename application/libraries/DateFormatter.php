<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class DateFormatter {
	function format($date, $day = false, $time = false, $lang = 'id') {
		$current_date = strtotime($date);
		$base_date = $lang==='id'?date('d-m-Y', $current_date):date('Y-m-d', $current_date);
		$base_time = $time!==false?date(' H:i', $current_date):'';

		if ($day !== false && $lang == 'id') {
			$day_name = date('l', $current_date);
			$base_day = $this->change_day_name($day_name).', ';
		} else {
			$base_day = '';
		}

		return $base_day.$base_date.$base_time;
	}

	function change_day_name($day) {
		$converted = null;
		switch ($day) {
			case 'Sunday':
				$converted = 'Minggu';
				break;
			case 'Monday':
				$converted = 'Senin';
				break;
			case 'Tuesday':
				$converted = 'Selasa';
				break;
			case 'Wednesday';
				$converted = 'Rabu';
				break;
			case 'Thursday':
				$converted = 'Kamis';
				break;
			case 'Friday':
				$converted = 'Jum\'at';
				break;
			case 'Saturday':
				$converted = 'Sabtu';
				break;
			default:
				break;
		}

		return $converted;
	}
}

/* End of file Template.php */
/* Location: ./system/application/libraries/Template.php */